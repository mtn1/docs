### Contributing to Commento

#### Introduction

Thank you for choosing to contribute to Commento. There are, however, certain guidelines we'd like for the contributors to follow. If you're a newcomer to open source and you haven't contributed to other projects or used [Git](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html) before, you should consult the [newcomer docs](newcomers.md) first.

#### How do I start contributing?

 - Before you start contributing to Commento, you must sign our [Developer Certificate of Origin](https://dco.commento.io). This is a document certifying that any contributions you make to the project are your own or that you have the right to submit them. Read the document carefully.
 - Once that's out of the way, you should [set up the environment](dev-env.md) required to compile and build the project.
 - Then, you should [fork](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html) the [project](https://gitlab.com/commento/commento-ce) and [clone](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html#clone-your-project) your fork locally. All your development will happen on your fork.
 - Following this, you should then compile the project [from source](installation-source.md) and make sure everything is working as intended. [Run the tests](running-tests.md) to make sure everything is working as intended.
 - Next, you should review the [development guidelines](development-guidelines.md); code standards (commit messages are code too) are important if you plan on reading what you've written on a different day.
 - After you've read the guidelines, you pick and work on the contribution you want to make. This can be a bug fix, a new feature, a performance improvement, or a code standards improvement, or even a typo. If you need inspiration, pick an issue from [our public issue tracker](https://gitlab.com/commento/commento-ce/issues).
 - When you're satisfied with your work (and when you're sure you've adhered to the guidelines), you can [make a Merge Request (MR)](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).

#### I've made a mess of it

In the [words](https://www.kernel.org/doc/html/v4.10/process/coding-style.html#you-ve-made-a-mess-of-it) of Linus Torvalds, that's okay, we all do. [Join us in IRC](https://irc.commento.io), and we'll help you out. Make sure you've read the documentation beforehand.
